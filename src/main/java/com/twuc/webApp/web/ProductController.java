package com.twuc.webApp.web;

import com.twuc.webApp.contract.CreateProductRequest;
import com.twuc.webApp.domain.Product;
import com.twuc.webApp.domain.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class ProductController {
    @Autowired
    private ProductRepository productRepository;


    @PostMapping("/products")
    public ResponseEntity<Product> createProducts(@RequestBody @Valid Product product1){
        Product product = productRepository
                .save(product1);
        productRepository.flush();


        return ResponseEntity.status(201).
                header("location","http://localhost/api/products/"+product.getId())
                .build();
    }

    @GetMapping("/products/{productId}")
    public ResponseEntity<Product> queryProducts(@PathVariable  Long productId){
        Optional<Product> product = productRepository.findById(productId);
        if(!product.isPresent()){
            return ResponseEntity.status(404).build();
        }
        return ResponseEntity.status(200).contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(product.get());
    }


}